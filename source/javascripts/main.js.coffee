Molecule(
  width: 1280
  height: 640
).tilemap("city_bg", "images/bg_city.json").ready (game) ->
  game.boundaries =
    x: 0
    y: 0
    width: game.width
    height: game.height

  game.input.enable "keyboard"

  game.molecule.define "Bird",
    ax: 1
    ay: 9.78
    init: ->

    update: ->
      if @sprite.collision.map.down or @sprite.collision.boundaries.down or @sprite.collision.sprite.down
        @ax = 0
        @ay = - 9.78
      else if @sprite.collision.map.up or @sprite.collision.boundaries.up or @sprite.collision.sprite.up
        @ax = 0
        @ay = 9.78
      else if @sprite.collision.map.right or @sprite.collision.boundaries.right or @sprite.collision.sprite.right
        @ax = - 9.78
        @ay = 0
      else if @sprite.collision.map.left or @sprite.collision.boundaries.left or @sprite.collision.sprite.left
        @ax = 9.78
        @ay = 0
      if game.input.key.SPACE
        @ax = @ax - 1
        @ay = @ay + 1
        
      @sprite.acceleration.x = @ax
      @sprite.acceleration.y = @ay
      

  game.tilemap.set "city_bg"
    
